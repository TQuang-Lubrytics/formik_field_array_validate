import React, { Component } from 'react';
import Select from 'react-select';

const products = [
    {value:1,label:"A"},
    {value:2,label:"B"},
    {value:3,label:"C"},
    {value:4,label:"D"},
    {value:5,label:"E"},
]

class SelectComponent extends Component {
    handleChange = value => {
      this.props.onChange(this.props.field.name, value);
    };
  
    handleBlur = () => {
      this.props.onBlur(this.props.field.name, true);
    };
  
    render() {
      return (
        <div style={{ margin: '1rem 0 0' }}>
          <Select
            options={products}
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            value={this.props.value}
            name={this.props.field.name}
          />
        </div>
      );
    }
  }

  export default SelectComponent;