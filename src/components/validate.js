import * as yup from 'yup';

const schema = () => yup.object().shape({
    products: yup.array()
    .of(
        yup.object().shape({
        product: yup.object().shape({
            value: yup.string().required("Hãy chọn sản phẩm")
        }),
        quantity: yup.number()
            .typeError("Hãy nhập số lượng")
            .integer("Số lượng phải là số nguyên")
            .min(1,"Số lượng phải lớn hơn 0")
        })
    )
    .required('Must have products')
});

export default schema;