import React from 'react';
import SelectField from '../components/SelectComponent';
import {Field, getIn} from 'formik';

const ErrorMessage = ({ name }) => {
    let lastKey = name.split('.').pop();
    return(
        <Field
            name={name}
            render={({ form }) => {
                const error = getIn(form.errors, name);
                const touch = getIn(form.touched, name);
                
                return renderErrorText(touch,error,lastKey);
            }}
        />
    )
}

const renderErrorText = (touch,error,lastKey) => {
    if(lastKey === "product" && touch && error){
        if(error.value){
            return <div className="help-block">{error.value}</div>
        }
        return null
    }
    if(lastKey !== "product" && touch && error){
        return <div className="help-block">{error}</div>
    }
    else{
        return null
    }
}

const OrderForm = props => {
    return (
        <div className="field-box">
            <Field 
                component={SelectField}
                name={"products."+props.index+".product"}
                onChange={props.setFieldValue}
                onBlur={props.setFieldTouched}
                value={props.values.products[props.index].product}
            />
            <ErrorMessage name={"products."+props.index+".product"} />

            <Field
                className="quantity"
                name={"products."+props.index+".quantity"}
            />
            <ErrorMessage name={"products."+props.index+".quantity"} />

            {props.removeable && <button type="button" onClick={()=>props.remove(props.index)} stt={props.stt} className="remove-product btn btn-danger btn-sm">Xóa sản phẩm</button>}
        </div>
    );
}

export default OrderForm;