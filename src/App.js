import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

const Loading = () => {
  return null;
}

const Order = Loadable({
  loader: () => import('./containers/Order'),
  loading: Loading
});

const Summary = Loadable({
  loader: () => import('./containers/Summary'),
  loading: Loading
});

const AppRouter = (props) => {
  return (
    <div className="app-container container-fluid">
      <div className="row">
        <section className="app clearfix">
          <Switch key={props.location.pathname + props.location.search} location={props.location}>
            <Route 
              exact path="/"
              render={() => (
                <Redirect to={"/order"}/>
              )}
            />
            <Route 
              path="/order"
              component={Order}
            />
            <Route 
              path="/summary"
              component={Summary}
            />
          </Switch>
        </section>
      </div>
    </div>
  );
}

class App extends Component {
  render() {
    return (
      <Router>
        <Route 
          path="/" 
          component={AppRouter}
        />
      </Router>
    );
  }
}

export default App;
