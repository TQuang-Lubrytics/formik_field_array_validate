import React, { Component } from 'react';
import OrderForm from '../components/OrderForm';
import {Formik, FieldArray} from 'formik';
import schema from '../components/validate';

class Order extends Component {
    constructor(props){
        super(props);
        this.state = {
            products:[]
        }
    }
    mapProducts = (props) => {
        return (
            <FieldArray name="products" render={
                fieldArrayProps => {
                    return (
                        <React.Fragment>
                            {
                                props.values.products.map((p,index)=>{
                                    return <OrderForm key={index} index={index} removeable={props.values.products.length > 1} {...fieldArrayProps} {...props}/>
                                })
                            }
                            <button type="button" onClick={()=>fieldArrayProps.push({product:{},quantity:''})} className="btn btn-link">Thêm sản phẩm</button>
                        </React.Fragment>
                    )
                }
            }/>
        )
    }
    
    render() {
        return (
            <div className="order-container container">
                <Formik 
                    initialValues={{ 
                        products: [
                            {product:{},quantity:''}
                        ] 
                    }}
                    validationSchema={schema}
                    onSubmit={values => {
                        this.setState({products:values})
                    }}
                    render={(props) => (
                        <React.Fragment>
                            <div className="header">
                                <button type="button" onClick={props.handleSubmit} className="btn btn-sm btn-primary next pull-right">next</button>
                            </div>
                            <div className="order-form">
                                <form className="product-form">
                                {
                                    this.mapProducts(props)
                                }
                                </form>
                            </div>
                        </React.Fragment>
                    )} 
                />
            </div>
        );
    }
}

export default Order;